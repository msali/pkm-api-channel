This is my implementation of an app fetching data from https://pokeapi.co/api/ api.

Target iOS SDK is iOS 11.0.

The UI is implemented without using storyboards or xibs.

Application main UIViewController is linked programmatically at startup supporting both: 

-iOS 13.0 version or higher, linked through SceneDelegate
-iOS versions lower than 13.0 (11.0 minimum supported). Application mai view controller is linked through AppDelegate to the UIWindow at startup.

Application follows MVVM partner. Related modules can be found in the directories:
- Model:
    Containing domain objects parsing api calls data. Domain classes implement Decodable protocol in order to be easily parsed by means of the JSONDecoder.
- ViewModel
    Containing ViewModel logic linking domain data to their UI representation.
    In order to do so, classes belonging to this module are responsible:
    - to perform remote api call and filling with the retrieved data the domain objects
    - expose to the view controllers belonging to the view layer the appropriate link to the their UI representation
- View
    The classes belonging to this layer are view controllers reponsible for building the UI Layout.
    UI is built making use of a reactive pattern, binding the UI setup to an Observable variable exposed by the view model.
    Observable pattern is created making use of the RXSwift framework.
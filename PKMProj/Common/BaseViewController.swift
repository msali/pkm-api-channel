//
//  BaseViewController.swift
//  pkmproject
//
//  Created by Salierno Mario on 30/11/2020.
//  Copyright © 2020 Salierno Mario. All rights reserved.
//

import Foundation
import UIKit


class BaseViewController: UIViewController {
    
    var activityIndicator = UIActivityIndicatorView(style: .gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        //activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        //activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        view.addSubview(activityIndicator)
    }
    
    
    func startLoading() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
        }
    }
    
    func stopLoading() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
    
}



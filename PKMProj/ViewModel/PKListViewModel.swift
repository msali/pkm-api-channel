//
//  PKListViewModel.swift
//  PKList
//
//  Created by Salierno Mario on 28/11/2020.
//  Copyright © 2020 Salierno Mario. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class PokemonListViewModel {
    
    var spinner = UIActivityIndicatorView(style: .gray)

    private let url = "https://pokeapi.co/api/v2/pokemon?limit=151"
    
    let list: BehaviorRelay<PokemonAPIList> = BehaviorRelay(value: PokemonAPIList(entries: []))
    
    // singleton
    static let shared = PokemonListViewModel()

    private init() {
        
    }
    
    
    func fetchPokemonList() {
        
        if let rUrl = URL(string: url) {
            URLSession.shared.dataTask(with: rUrl) { (data, _, _) in
                        guard let data = data else {
                                return
                        }
                        
                        if let pkList = try? JSONDecoder().decode(PokemonAPIList.self, from: data) {
                            self.list.accept(pkList)
                        }
                        

            //            DispatchQueue.main.async {
            //                self.pokemonList = pokemonList
            ////                let res = pokemonList.results
            ////                let res1 = res[0]
            ////                let resin = res[0].name
            ////                let res2 = res[1]
            //            }
                    }.resume()
        }
        
        
    }
    

    
}


class PokemonListEntryViewModel: Identifiable {
    
    let id = UUID()
    
    let item: PokemonListEntry
    
    init(pkItem: PokemonListEntry) {
        self.item = pkItem
    }
    
    var name: String {
        return self.item.name
    }
    
    var url: String {
        return self.item.url
    }
    
    
}

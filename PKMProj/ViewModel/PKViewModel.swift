//
//  PKViewModel.swift
//  PKList
//
//  Created by Salierno Mario on 28/11/2020.
//  Copyright © 2020 Salierno Mario. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

enum PKDetailCellType: CaseIterable {
    case title
    case image
    case imageFD
    case imageBD
    case imageFS
    case imageBS
    case type
    case hp
    case atk
    case def
    case specAtk
    case specDef
    case speed
}


class PokemonViewModel {
    
    let pkModel: BehaviorRelay<PokemonDetail> = BehaviorRelay(value: PokemonDetail())
    
    static let shared = PokemonViewModel()

    private init() {
        
    }
    
    func fetchPokemonDetails(urlStr: String) {
        
        
        guard let url = URL(string: urlStr) else { return }
                
                URLSession.shared.dataTask(with: url) { (data, _, _) in
                    guard let data = data else {
                            return
                    }
                    
                    if let pkMod = try? JSONDecoder().decode(PokemonDetail.self, from: data) {
                        self.pkModel.accept(pkMod)
                    }
                    

        //            DispatchQueue.main.async {
        //                self.pokemonList = pokemonList
        ////                let res = pokemonList.results
        ////                let res1 = res[0]
        ////                let resin = res[0].name
        ////                let res2 = res[1]
        //            }
                }.resume()
        
    }
    
    func getCell(collectionView: UICollectionView, indexPath: IndexPath, cellType: PKDetailCellType) -> UICollectionViewCell {
        switch cellType {
            
        case .title:
            if let c = collectionView.dequeueReusableCell(withReuseIdentifier: "TitleCell", for: indexPath) as? TitleCell {
                c.titleLabel.text = pkModel.value.name.capitalized
                return c
            }
        case .image, .imageFD:
            if let c = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as? ImageCell {
                c.image.downloaded(from: pkModel.value.sprites.front_default)
                return c
            }
        case .imageBD:
            if let c = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as? ImageCell {
                c.image.downloaded(from: pkModel.value.sprites.back_default)
                return c
            }
        case .imageFS:
            if let c = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as? ImageCell {
                c.image.downloaded(from: pkModel.value.sprites.front_shiny)
                return c
            }
        case .imageBS:
            if let c = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as? ImageCell {
                c.image.downloaded(from: pkModel.value.sprites.back_shiny)
                return c
            }
        case .type:
            if let c = collectionView.dequeueReusableCell(withReuseIdentifier: "TitleCell", for: indexPath) as? TitleCell {
                if let t = pkModel.value.types.first {
                    c.titleLabel.textAlignment = .left
                    c.titleLabel.text = "Type: " + t.type.name.capitalized
                }
                
                return c
            }
        case .hp:
            if let c = collectionView.dequeueReusableCell(withReuseIdentifier: "StatCell", for: indexPath) as? StatCell {
                
                let val = pkModel.value.stats.filter({ $0.stat.name == "hp" }).first?.base_stat
                
                c.titleLabel.text = "HP: " + String(val ?? 0)
                return c
            }
        case .atk:
            if let c = collectionView.dequeueReusableCell(withReuseIdentifier: "StatCell", for: indexPath) as? StatCell {
                
                let val = pkModel.value.stats.filter({ $0.stat.name == "attack" }).first?.base_stat
                
                c.titleLabel.text = "Attack: " + String(val ?? 0)
                return c
            }
        case .def:
            if let c = collectionView.dequeueReusableCell(withReuseIdentifier: "StatCell", for: indexPath) as? StatCell {
                
                let val = pkModel.value.stats.filter({ $0.stat.name == "defense" }).first?.base_stat
                
                c.titleLabel.text = "Defense: " + String(val ?? 0)
                return c
            }
        case .specAtk:
            if let c = collectionView.dequeueReusableCell(withReuseIdentifier: "StatCell", for: indexPath) as? StatCell {
                
                let val = pkModel.value.stats.filter({ $0.stat.name == "special-attack" }).first?.base_stat
                
                c.titleLabel.text = "Spec.Att.: " + String(val ?? 0)
                return c
            }
        case .specDef:
            if let c = collectionView.dequeueReusableCell(withReuseIdentifier: "StatCell", for: indexPath) as? StatCell {
                
                let val = pkModel.value.stats.filter({ $0.stat.name == "special-defense" }).first?.base_stat
                
                c.titleLabel.text = "Spec.Def.: " + String(val ?? 0)
                return c
            }
        case .speed:
            if let c = collectionView.dequeueReusableCell(withReuseIdentifier: "StatCell", for: indexPath) as? StatCell {
                
                let val = pkModel.value.stats.filter({ $0.stat.name == "speed" }).first?.base_stat
                
                c.titleLabel.text = "Speed: " + String(val ?? 0)
                return c
            }
        }
        
        return UICollectionViewCell()
    }
    
    func getCellSize(fullWidth: CGFloat, cellType: PKDetailCellType) -> CGSize {
        switch cellType {
            
        case .title:
            return CGSize(width: fullWidth, height: 40.0)
        case .image:
            return CGSize(width: fullWidth, height: 140.0)
        case .type:
            return CGSize(width: fullWidth, height: 50.0)
        case .hp,.atk,.def,.specAtk,.specDef,.speed:
            return CGSize(width: fullWidth, height: 30.0)
        case .imageFD,.imageBD,.imageFS,.imageBS:
            return CGSize(width: fullWidth/4, height: 100.0)
        }
        
        //return CGSize(width: 0.0, height: 0.0)
    }
    

    
    
}


//
//  PKModel.swift
//  PKList
//
//  Created by Salierno Mario on 28/11/2020.
//  Copyright © 2020 Salierno Mario. All rights reserved.
//

import Foundation


struct PokemonAPIList : Decodable {
    var results: [PokemonListEntry] = []
    
    init(entries: [PokemonListEntry] ) {
        results = entries
    }
}

struct PokemonListEntry : Decodable, Hashable {
    var name: String
    var url: String
}

struct PokemonDetail : Decodable {
    var name: String
    var sprites: PokemonImageSet
    var stats: [PokemonStat]
    var types: [PokemonType]
    
    init(){
        name = ""
        sprites = PokemonImageSet()
        stats = []
        types = []
    }
}



struct PokemonImageSet: Decodable {
    var back_default: String
    var back_shiny: String
    var front_default: String
    var front_shiny: String
    
    init(){
        back_default = ""
        back_shiny = ""
        front_default = ""
        front_shiny = ""
    }
}


struct PokemonStat: Decodable, Hashable {
    var base_stat: Int
    var stat: PKStat
}

struct PKStat: Decodable, Hashable {
    var name: String
}

/*
 "types": [
   {
     "slot": 1,
     "type": {
       "name": "fire",
       "url": "https://pokeapi.co/api/v2/type/10/"
     }
   }
 ],
 */
struct PokemonType: Decodable, Hashable {
    var type: PokemonTypeCouple
    var slot: Int
}

struct PokemonTypeCouple: Decodable, Hashable {
    var name: String
    var url: String
}

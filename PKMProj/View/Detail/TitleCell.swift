//
//  TitleCell.swift
//  pkmproject
//
//  Created by Salierno Mario on 29/11/2020.
//  Copyright © 2020 Salierno Mario. All rights reserved.
//

import Foundation
import UIKit

class TitleCell: UICollectionViewCell {
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        setupUI()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Properties
    lazy var containerBackgroundView: UIView = {
        let view = UIView()
        //view.layer.borderColor = UIColor.systemGray.cgColor
        view.backgroundColor = .red
        view.layer.borderWidth = 1
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 20)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private func setupUI() {
        self.contentView.addSubview(containerBackgroundView)
        containerBackgroundView.addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            containerBackgroundView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 5),
            containerBackgroundView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -5),
            containerBackgroundView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 5),
            containerBackgroundView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -5),
            titleLabel.centerXAnchor.constraint(equalTo: containerBackgroundView.centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: containerBackgroundView.centerYAnchor)
        ])
        
    }
    
}

//
//  ImageCell.swift
//  pkmproject
//
//  Created by Salierno Mario on 30/11/2020.
//  Copyright © 2020 Salierno Mario. All rights reserved.
//

import Foundation
import UIKit

class ImageCell: UICollectionViewCell {
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        setupUI()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Properties
    lazy var containerBackgroundView: UIView = {
        let view = UIView()
        //view.layer.borderColor = UIColor.systemGray.cgColor
        view.backgroundColor = .white
        view.layer.borderWidth = 1
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var image: UIImageView = {
        let imgV = UIImageView()
        imgV.translatesAutoresizingMaskIntoConstraints = false
        return imgV
    }()
    
    private func setupUI() {
        self.contentView.addSubview(containerBackgroundView)
        containerBackgroundView.addSubview(image)
        
        NSLayoutConstraint.activate([
            containerBackgroundView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 5),
            containerBackgroundView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -5),
            containerBackgroundView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 5),
            containerBackgroundView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -5),
            image.centerXAnchor.constraint(equalTo: containerBackgroundView.centerXAnchor),
            image.centerYAnchor.constraint(equalTo: containerBackgroundView.centerYAnchor)
        ])
        
    }
    
}


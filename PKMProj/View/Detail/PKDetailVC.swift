//
//  PKDetail.swift
//  pkmproject
//
//  Created by Salierno Mario on 29/11/2020.
//  Copyright © 2020 Salierno Mario. All rights reserved.
//

import Foundation

import UIKit
import RxSwift
import RxCocoa

class PKDetailVC: BaseViewController {
    
    private let disposeBag = DisposeBag()
    
    var pkEntry: PokemonListEntry?
    // MARK: - Properties
    var cells: [PKDetailCellType] = []
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        if let urlStr = pkEntry?.url {
            self.startLoading()
            PokemonViewModel.shared.fetchPokemonDetails(urlStr: urlStr)
        }
        
        PokemonViewModel.shared.pkModel.asObservable()
         .observeOn(MainScheduler.instance)
         .skip(1)
         .subscribe(onNext: {  (model) in
             self.stopLoading()
             self.cells = PKDetailCellType.allCases
             self.collectionView.reloadData()
         }).disposed(by: disposeBag)
            
            
//        .subscribe(onNext: {
//          [unowned self] model in
//            self.pkModel = model
//            self.cells = PKDetailCellType.allCases
//            self.collectionView.reloadData()
//            })
//        .disposed(by: disposeBag)
    }
    
    
    
    lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout())
        collectionView.register(TitleCell.self, forCellWithReuseIdentifier: "TitleCell")
        collectionView.register(ImageCell.self, forCellWithReuseIdentifier: "ImageCell")
        collectionView.register(StatCell.self, forCellWithReuseIdentifier: "StatCell")
        collectionView.backgroundColor = .white
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
}

// MARK: - UI Setup
extension PKDetailVC {
    private func setupUI() {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        self.view.backgroundColor = .white
        
        self.view.addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            collectionView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])
    }
    
    private func collectionViewLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewFlowLayout()
        let cellWidthHeightConstant: CGFloat = UIScreen.main.bounds.width //* 0.2

        layout.sectionInset = UIEdgeInsets(top: 0,
                                           left: 10,
                                           bottom: 0,
                                           right: 10)
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.itemSize = CGSize(width: cellWidthHeightConstant, height: cellWidthHeightConstant)
        
        return layout
    }
}

// MARK: - UICollectionViewDelegate & Data Source
extension PKDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cells.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if cells.count > indexPath.row {
           let celtype = cells[indexPath.row]
           return PokemonViewModel.shared.getCell(collectionView: collectionView, indexPath: indexPath, cellType: celtype)
        }
       
        return UICollectionViewCell()
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if cells.count > indexPath.row {
           let celtype = cells[indexPath.row]
            return PokemonViewModel.shared.getCellSize(fullWidth: collectionView.frame.width - 20.0, cellType: celtype)
        }
        
        return CGSize(width: 0.0, height: 0.0)
    }
    
}

//
//  PKListVC.swift
//  pkmproject
//
//  Created by Salierno Mario on 29/11/2020.
//  Copyright © 2020 Salierno Mario. All rights reserved.
//

import Foundation
import UIKit
import RxSwift


class PKListViewController: BaseViewController {
    
    private let disposeBag = DisposeBag()
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavBar()
        
        self.startLoading()
        
        PokemonListViewModel.shared.fetchPokemonList()
        
        PokemonListViewModel.shared.list.asObservable()
            .observeOn(MainScheduler.instance)
            .skip(1)
            .subscribe(onNext: {  (list) in
            self.pkList = list
            self.listDidLoad()
            }).disposed(by: disposeBag)
            
    }
    
    // MARK: - Properties
    var pkList: PokemonAPIList?

    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(PKListCell.self, forCellReuseIdentifier: "PKListCell")
        tableView.tableFooterView = UIView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
}
extension PKListViewController {
    
    private func setupUI() {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        self.view.backgroundColor = .red
        
        self.view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.widthAnchor.constraint(equalTo: self.view.widthAnchor),
            tableView.heightAnchor.constraint(equalTo: self.view.heightAnchor)
        ])
    }
    
    func setUpNavBar(){
        if let nav = self.navigationController?.navigationBar {
            nav.tintColor = UIColor.white
            nav.barTintColor = .red
            nav.topItem?.title = "Pokemon"
            //nav.topItem?.tintColor = UIColor.white
            nav.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        }
        
    }
    
    func listDidLoad(){
        self.stopLoading()
        self.setupUI()
        self.tableView.reloadData()
    }
}
// MARK: - UITableViewDelegate & DataSource
extension PKListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pkList?.results.count ?? 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PKListCell") as! PKListCell
        cell.titleLabel.text = pkList?.results[indexPath.row].name.capitalized
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedEntry = pkList?.results[indexPath.row] {
            let detailVC = PKDetailVC()
            detailVC.pkEntry = selectedEntry
            navigationController?.pushViewController(detailVC, animated: true)
        }
    }
    
}
